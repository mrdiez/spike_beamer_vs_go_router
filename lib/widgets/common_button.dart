import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/config/theme/sizes.dart';

class CommonButton extends StatelessWidget {
  const CommonButton({Key? key, required this.action, required this.label}) : super(key: key);

  final void Function()? action;
  final String label;

  @override
  Widget build(context) => Padding(
        padding: Sizes.padding(xs),
        child: ElevatedButton(
          onPressed: action,
          child: Text(label),
        ),
      );
}
