extension FunctionTool on Function? {
  void executeIfNotNull() {
    if (this != null) this!();
  }
}
