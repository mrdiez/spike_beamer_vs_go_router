import 'package:dartx/dartx.dart';

extension MapTool<T1, T2> on Map<T1, T2> {
  List<T1> get keyList => keys.toList();
  List<T2> get valueList => values.toList();
}

extension IterableTool<T> on Iterable<T> {
  int indexOf(T item) => toList().indexOf(item);

  int indexOfWhere(bool Function(T) where, {int orElse = -1}) {
    final item = firstOrNullWhere(where);
    if (item == null) return orElse;
    final index = indexOf(item);
    return index < 0 ? orElse : index;
  }

  T? get beforeLast => length >= 2 ? this[length - 2] : null;

  T operator [](int i) => toList()[i];

  List<E> mapList<E>(E Function(T) toElement) => map(toElement).toList();
  List<E> mapListIndexed<E>(E Function(int, T) toElement) => mapIndexed(toElement).toList();

  Map<E1, E2> toMap<E1, E2>(MapEntry<E1, E2> Function(T) entryBuilder) => Map.fromEntries(map(entryBuilder));
}
