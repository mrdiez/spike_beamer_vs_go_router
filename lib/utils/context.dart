import 'package:flutter/material.dart';

extension ContextTool on BuildContext {
  ThemeData get theme => Theme.of(this);
  BottomNavigationBarThemeData get bottomNavigationBarTheme => theme.bottomNavigationBarTheme;
}
