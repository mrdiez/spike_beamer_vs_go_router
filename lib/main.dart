import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:spike_beamer_vs_go_router/config/environment.dart';
import 'package:spike_beamer_vs_go_router/config/theme/theme.dart';
import 'package:spike_beamer_vs_go_router/services/router_switcher.dart';

import 'config/routes.dart';
import 'config/theme/colors.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widgets is the root of your application.
  @override
  Widget build(context, ref) {
    final _router = ref.watch(RouterSwitcher.instance(context));
    final _primaryColor = Environment.router == CustomRouter.goRouter ? redPill : bluePill;
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: defaultTheme(_primaryColor),
      routeInformationParser: _router.informationParser,
      routerDelegate: _router.delegate,
    );
  }
}
