import 'package:dartx/dartx.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';
import 'package:spike_beamer_vs_go_router/config/theme/sizes.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/data/mocks/mocked_customers.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/presentation/widgets/customer_title.dart';
import 'package:spike_beamer_vs_go_router/services/router_switcher.dart';
import 'package:spike_beamer_vs_go_router/widgets/common_button.dart';

class CustomerListScreen extends ConsumerWidget {
  static const String route = 'customers';
  const CustomerListScreen({Key? key}) : super(key: key);
  String getParentRoute(RouterModel router) {
    switch (router.type) {
      case CustomRouter.beamer:
        return route;
      default:
        return router.delegate.currentConfiguration.toString();
    }
  }

  @override
  Widget build(context, ref) {
    final _router = ref.read(RouterSwitcher.instance(context));
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CustomerTitle(),
          Sizes.gap(l),
          ...mockedCustomers.mapIndexed((i, customer) => CommonButton(
                action: () => _router.navigate('${getParentRoute(_router)}/$i'),
                label: customer,
              )),
        ],
      ),
    );
  }
}
