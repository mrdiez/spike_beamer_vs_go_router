import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/data/mocks/mocked_customers.dart';
import 'package:spike_beamer_vs_go_router/services/router_switcher.dart';
import 'package:spike_beamer_vs_go_router/widgets/common_button.dart';

class CustomerDetailsScreen extends ConsumerWidget {
  const CustomerDetailsScreen({Key? key, required this.id}) : super(key: key);

  final int id;

  @override
  Widget build(context, ref) {
    final _router = ref.read(RouterSwitcher.instance(context));
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Hello ${mockedCustomers[id]}'),
          CommonButton(
            action: _router.pop != null ? () => _router.pop!() : null,
            label: 'Back',
          ),
        ],
      ),
    );
  }
}
