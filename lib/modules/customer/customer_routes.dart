import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/presentation/screens/customer_list.dart';

import 'data/mocks/mocked_customers.dart';
import 'presentation/screens/customer_details.dart';

// Beamer routes declaration
class CustomerBeamerRoute extends CustomBeamLocation {
  @override
  List<BeamScreen> get screens => [
        BeamScreen(
          path: CustomerListScreen.route,
          builder: (_) => const CustomerListScreen(),
        ),
        BeamScreen(
          path: '${CustomerListScreen.route}/:id',
          builder: _customerDetailsBeamerPageBuilder,
          condition: (state) => state.pathParameters.containsKey('id'),
          paramsAdapter: (params) => params!['id'],
        ),
      ];
}

// Customer's details widget builder for beamer routing
Widget _customerDetailsBeamerPageBuilder(dynamic id) {
  final _id = int.parse(id!.toString());
  if (_id < 0 || _id >= mockedCustomers.length) return notFoundScreen;
  return CustomerDetailsScreen(key: ValueKey(_id), id: _id);
}
