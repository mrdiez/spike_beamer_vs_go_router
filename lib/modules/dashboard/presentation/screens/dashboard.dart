import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/modules/dashboard/presentation/widgets/dashboard_title.dart';

class DashboardScreen extends StatelessWidget {
  static const String route = 'dashboard';
  const DashboardScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) => const Material(child: Center(child: DashboardTitle()));
}
