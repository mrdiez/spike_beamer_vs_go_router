import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Not really useful, only here for app's architecture demo
class AuthTitle extends StatelessWidget {
  const AuthTitle(this.text, {Key? key}) : super(key: key);

  final String text;

  @override
  Widget build(context) => Text(text);
}
