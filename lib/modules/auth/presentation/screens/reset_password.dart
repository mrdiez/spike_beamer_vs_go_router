import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/modules/auth/presentation/widgets/auth_title.dart';
import 'package:spike_beamer_vs_go_router/widgets/common_button.dart';

class ResetPasswordScreen extends StatelessWidget {
  static const String route = 'reset_password';
  const ResetPasswordScreen({Key? key}) : super(key: key);
  @override
  Widget build(context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const AuthTitle('Reset Password'),
          CommonButton(
            action: () => Navigator.of(context).pop(),
            label: 'Back',
          ),
        ],
      ),
    );
  }
}
