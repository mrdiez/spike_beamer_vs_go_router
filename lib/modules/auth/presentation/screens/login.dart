import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:spike_beamer_vs_go_router/modules/auth/auth_routes.dart';
import 'package:spike_beamer_vs_go_router/modules/auth/presentation/widgets/auth_title.dart';
import 'package:spike_beamer_vs_go_router/modules/home/presentation/screens/home.dart';
import 'package:spike_beamer_vs_go_router/services/router_switcher.dart';
import 'package:spike_beamer_vs_go_router/widgets/common_button.dart';

class LoginScreen extends ConsumerWidget {
  static const String route = 'login';
  const LoginScreen({Key? key}) : super(key: key);
  @override
  Widget build(context, ref) {
    final _router = ref.read(RouterSwitcher.instance(context));
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const AuthTitle('LOGIN'),
          CommonButton(
            action: () => _router.navigate('/${HomeScreen.route}/${HomeScreen.tabs.first.route}'),
            label: 'Log in',
          ),
          CommonButton(
            action: () => _router.navigate(resetPasswordRoute),
            label: 'Forgot password ?',
          ),
          CommonButton(
            action: () => _router.navigate(signUpRoute),
            label: 'Sign up',
          ),
        ],
      ),
    );
  }
}
