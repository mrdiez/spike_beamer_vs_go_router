import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';

import 'presentation/screens/login.dart';
import 'presentation/screens/reset_password.dart';
import 'presentation/screens/sign_up.dart';

const String loginRoute = '/${LoginScreen.route}';
const String resetPasswordRoute = '$loginRoute/${ResetPasswordScreen.route}';
const String signUpRoute = '$loginRoute/${SignUpScreen.route}';

// Go_router routes declaration
final authGoRoutes = [
  GoRoute(
    path: loginRoute,
    pageBuilder: (context, state) => const MaterialPage(child: LoginScreen()),
    routes: [
      GoRoute(
        path: ResetPasswordScreen.route,
        pageBuilder: (context, state) => const MaterialPage(child: ResetPasswordScreen()),
      ),
      GoRoute(
        path: SignUpScreen.route,
        pageBuilder: (context, state) => const MaterialPage(child: SignUpScreen()),
      ),
    ],
  ),
];

// Beamer routes declaration
class AuthBeamerRoute extends CustomBeamLocation {
  @override
  List<BeamGuard> get guards => [
        BeamGuard(pathBlueprints: ['/'], check: (_, __) => false, beamToNamed: loginRoute),
      ];
  @override
  List<BeamScreen> get screens => [
        BeamScreen(path: loginRoute, builder: (_) => const LoginScreen()),
        BeamScreen(path: resetPasswordRoute, builder: (_) => const ResetPasswordScreen()),
        BeamScreen(path: signUpRoute, builder: (_) => const SignUpScreen()),
      ];
}
