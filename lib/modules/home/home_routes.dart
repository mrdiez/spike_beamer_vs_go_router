import 'package:beamer/beamer.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';
import 'package:spike_beamer_vs_go_router/modules/home/presentation/screens/home.dart';

const String homeRoute = '/${HomeScreen.route}';

// Get Home's tab index from route path
int _getTabIndex(String? route) => HomeScreen.tabs.indexWhere((tab) => tab.route.contains(route ?? ''));

// Go_router routes declaration
final homeGoRoutes = [
  GoRoute(
    path: homeRoute,
    redirect: (_) => '$homeRoute/${HomeScreen.tabs.first.route}',
  ),
  // ./home/dashboard, ./home/calendar & ./home/customers/:id
  ...HomeScreen.tabs.mapIndexed(
    (i, tab) => _homeGoRouteBuilder(
      tab: tab,
      parentRoute: homeRoute,
      builder: (view) => HomeScreen(key: const ValueKey(homeRoute), tabIndex: i),
    ),
  ),
  // ./dashboard, ./calendar & ./customers/:id
  ...HomeScreen.tabs.map(
    (tab) => _homeGoRouteBuilder(tab: tab),
  ),
];

// Home MaterialPage builder for go_router routing
GoRoute _homeGoRouteBuilder({
  required HomeTab tab,
  String? parentRoute,
  Widget Function(Widget)? builder,
  Widget Function(Widget)? nestedBuilder,
}) =>
    GoRoute(
        path: '${parentRoute ?? ''}/${tab.route}',
        pageBuilder: (context, state) => MaterialPage(
            key: ValueKey(parentRoute ?? tab.route),
            child: builder != null ? builder(tab.view(state.params)) : tab.view(state.params)),
        routes: [
          ...tab.nestedRoutes.map((nested) => GoRoute(
                path: nested.route,
                pageBuilder: (context, state) => MaterialPage(
                  key: ValueKey(nested.route),
                  child: nestedBuilder != null ? nestedBuilder(nested.view(state.params)) : nested.view(state.params),
                ),
              ))
        ]);

// Beamer routes declaration
class HomeBeamerRoute extends CustomBeamLocation {
  @override
  List<BeamGuard> get guards => [
        BeamGuard(
          pathBlueprints: [homeRoute],
          check: (context, location) => false,
          beamToNamed: '$homeRoute/${HomeScreen.tabs.first.route}',
        ),
      ];
  @override
  List<BeamScreen> get screens => [
        // ./home/dashboard, ./home/calendar & ./home/customers
        BeamScreen(
          path: '$homeRoute/:route',
          builder: _homeBeamerPageBuilder,
          condition: (state) => state.pathParameters.containsKey('route'),
          paramsAdapter: (params) => params!['route'] as String,
        ),
      ];
}

// Home widget builder for beamer routing
Widget _homeBeamerPageBuilder(dynamic route) {
  final i = _getTabIndex(route.toString());
  if (i < 0) return notFoundScreen;
  return HomeScreen(key: ValueKey(route), tabIndex: i);
}
