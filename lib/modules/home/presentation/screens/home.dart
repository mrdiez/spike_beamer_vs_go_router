import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';
import 'package:spike_beamer_vs_go_router/modules/calendar/presentation/screens/calendar.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/data/mocks/mocked_customers.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/presentation/screens/customer_details.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/presentation/screens/customer_list.dart';
import 'package:spike_beamer_vs_go_router/modules/dashboard/presentation/screens/dashboard.dart';
import 'package:spike_beamer_vs_go_router/modules/home/home_routes.dart';
import 'package:spike_beamer_vs_go_router/modules/home/presentation/widgets/custom_bottom_navigation_bar.dart';
import 'package:spike_beamer_vs_go_router/modules/home/presentation/widgets/home_title.dart';
import 'package:spike_beamer_vs_go_router/services/router_switcher.dart';
import 'package:spike_beamer_vs_go_router/utils/iterable.dart';

class HomeTab {
  const HomeTab(
      {required this.title, required this.route, required this.icon, required this.view, this.nestedRoutes = const []});
  final String title;
  final String route;
  final IconData icon;
  final Widget Function(dynamic) view;
  final List<HomeTab> nestedRoutes;
  factory HomeTab.nested(String route, Widget Function(dynamic) view) =>
      HomeTab(title: '', route: route, icon: Icons.adb, view: view);
}

class HomeScreen extends ConsumerStatefulWidget {
  static const String route = 'home';

  static final List<HomeTab> tabs = [
    HomeTab(
        title: 'Dashboard', route: DashboardScreen.route, icon: Icons.dashboard, view: (_) => const DashboardScreen()),
    HomeTab(
        title: 'Calendar',
        route: CalendarScreen.route,
        icon: Icons.calendar_today,
        view: (_) => const CalendarScreen()),
    HomeTab(
      title: 'Customers',
      route: CustomerListScreen.route,
      icon: Icons.person,
      view: (_) => const CustomerListScreen(),
      nestedRoutes: [
        HomeTab.nested(':id', (params) {
          final id = int.parse(params['id'] ?? '-1');
          if (id < 0 && id >= mockedCustomers.length) return notFoundScreen;
          return CustomerDetailsScreen(id: id);
        })
      ],
    ),
  ];

  const HomeScreen({required this.tabIndex, Key? key}) : super(key: key);

  final int tabIndex;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> with TickerProviderStateMixin {
  late final TabController _tabController;
  late String _lastRoute;

  String get _tabRoute => HomeScreen.tabs[_tabController.index].route;
  RouterModel get _router => ref.read(RouterSwitcher.instance(context));

  void _onTabSwitch() {
    if (_lastRoute != _tabRoute) {
      _lastRoute = _tabRoute;
      _router.navigate('$homeRoute/$_lastRoute');
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: HomeScreen.tabs.length,
      vsync: this,
      initialIndex: widget.tabIndex,
    )..addListener(_onTabSwitch);
    _lastRoute = _tabRoute;
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _tabController.index = widget.tabIndex;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const HomeTitle(),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [for (final tab in HomeScreen.tabs) tab.view(null)],
        ),
        bottomNavigationBar: CustomBottomNavigationBar(
          tabController: _tabController,
          tabIcons: HomeScreen.tabs.toMap((tab) => MapEntry(tab.title, tab.icon)),
        ),
      );
}
