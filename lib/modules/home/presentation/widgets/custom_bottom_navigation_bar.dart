import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/utils/context.dart';
import 'package:spike_beamer_vs_go_router/utils/iterable.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  const CustomBottomNavigationBar({Key? key, required this.tabIcons, required this.tabController}) : super(key: key);

  final Map<String, IconData> tabIcons;
  final TabController tabController;

  @override
  Widget build(BuildContext context) {
    return ConvexAppBar(
      controller: tabController,
      activeColor: context.bottomNavigationBarTheme.selectedItemColor,
      color: context.bottomNavigationBarTheme.unselectedLabelStyle?.color,
      backgroundColor: context.bottomNavigationBarTheme.backgroundColor,
      items: tabIcons.keys.mapListIndexed(
        (i, title) => TabItem(
          title: title,
          icon: Icon(
            tabIcons[title],
            color: i == tabController.index
                ? context.bottomNavigationBarTheme.selectedIconTheme?.color
                : context.bottomNavigationBarTheme.unselectedIconTheme?.color,
          ),
        ),
      ),
    );
  }
}
