import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Not really useful, only here for app's architecture demo
class HomeTitle extends StatelessWidget {
  const HomeTitle({Key? key}) : super(key: key);

  @override
  Widget build(context) => const Text('HOME');
}
