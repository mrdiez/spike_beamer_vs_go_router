import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spike_beamer_vs_go_router/modules/calendar/presentation/widgets/calendar_title.dart';

class CalendarScreen extends StatelessWidget {
  static const String route = 'calendar';
  const CalendarScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) => const Material(child: Center(child: CalendarTitle()));
}
