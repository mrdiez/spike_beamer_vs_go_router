import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Not really useful, only here for app's architecture demo
class CalendarTitle extends StatelessWidget {
  const CalendarTitle({Key? key}) : super(key: key);

  @override
  Widget build(context) => const Text('CALENDAR');
}
