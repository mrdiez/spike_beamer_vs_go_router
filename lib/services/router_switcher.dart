import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:spike_beamer_vs_go_router/config/environment.dart';
import 'package:spike_beamer_vs_go_router/config/routes.dart';

part 'router_switcher.freezed.dart';

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class RouterModel with _$RouterModel {
  const factory RouterModel({
    required CustomRouter type,
    required RouteInformationParser<Object> informationParser,
    required RouterDelegate<Object> delegate,
    required void Function(String, [Map<String, String>]) navigate,
    required void Function()? pop,
  }) = _RouterModel;

  const RouterModel._();
}

class RouterSwitcher extends StateNotifier<RouterModel> {
  RouterSwitcher(this.context) : super(_getState(context));

  BuildContext context;

  static CustomRouter get _type => Environment.router;
  static set _type(CustomRouter value) => Environment.router = value;

  static final instance = StateNotifierProvider.family<RouterSwitcher, RouterModel, BuildContext>(
      (ref, context) => RouterSwitcher(context));

  static _getState(BuildContext context) => RouterModel(
        type: _type,
        informationParser: _routeInformationParser,
        delegate: _routerDelegate,
        navigate: _getNavigateMethod(context),
        pop: _getPopMethod(context),
      );

  void toggle() {
    if (_type == CustomRouter.goRouter) {
      _type = CustomRouter.beamer;
    } else {
      _type = CustomRouter.goRouter;
    }
    state = _getState(context);
  }

  static RouteInformationParser<Object> get _routeInformationParser {
    switch (_type) {
      case CustomRouter.beamer:
        return BeamerParser();
      default:
        return goRouter.routeInformationParser;
    }
  }

  static RouterDelegate<Object> get _routerDelegate {
    switch (_type) {
      case CustomRouter.beamer:
        return beamerRouterDelegate;
      default:
        return goRouter.routerDelegate;
    }
  }

  static void Function(String route, [Map<String, String> data]) _getNavigateMethod(BuildContext context) {
    return (String route, [Map<String, String> data = const {}]) {
      switch (_type) {
        case CustomRouter.beamer:
          return context.beamToNamed(route, data: data);
        default:
          return context.go(route);
      }
    };
  }

  static void Function()? _getPopMethod(BuildContext context) {
    return () {
      switch (_type) {
        case CustomRouter.beamer:
          return context.canBeamBack ? context.beamBack() : null;
        default:
          return Navigator.of(context).canPop() ? Navigator.of(context).pop() : null;
      }
    };
  }
}
