// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'router_switcher.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RouterModelTearOff {
  const _$RouterModelTearOff();

  _RouterModel call(
      {required CustomRouter type,
      required RouteInformationParser<Object> informationParser,
      required RouterDelegate<Object> delegate,
      required void Function(String, [Map<String, String>]) navigate,
      required void Function()? pop}) {
    return _RouterModel(
      type: type,
      informationParser: informationParser,
      delegate: delegate,
      navigate: navigate,
      pop: pop,
    );
  }
}

/// @nodoc
const $RouterModel = _$RouterModelTearOff();

/// @nodoc
mixin _$RouterModel {
  CustomRouter get type => throw _privateConstructorUsedError;
  RouteInformationParser<Object> get informationParser =>
      throw _privateConstructorUsedError;
  RouterDelegate<Object> get delegate => throw _privateConstructorUsedError;
  void Function(String, [Map<String, String>]) get navigate =>
      throw _privateConstructorUsedError;
  void Function()? get pop => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RouterModelCopyWith<RouterModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RouterModelCopyWith<$Res> {
  factory $RouterModelCopyWith(
          RouterModel value, $Res Function(RouterModel) then) =
      _$RouterModelCopyWithImpl<$Res>;
  $Res call(
      {CustomRouter type,
      RouteInformationParser<Object> informationParser,
      RouterDelegate<Object> delegate,
      void Function(String, [Map<String, String>]) navigate,
      void Function()? pop});
}

/// @nodoc
class _$RouterModelCopyWithImpl<$Res> implements $RouterModelCopyWith<$Res> {
  _$RouterModelCopyWithImpl(this._value, this._then);

  final RouterModel _value;
  // ignore: unused_field
  final $Res Function(RouterModel) _then;

  @override
  $Res call({
    Object? type = freezed,
    Object? informationParser = freezed,
    Object? delegate = freezed,
    Object? navigate = freezed,
    Object? pop = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as CustomRouter,
      informationParser: informationParser == freezed
          ? _value.informationParser
          : informationParser // ignore: cast_nullable_to_non_nullable
              as RouteInformationParser<Object>,
      delegate: delegate == freezed
          ? _value.delegate
          : delegate // ignore: cast_nullable_to_non_nullable
              as RouterDelegate<Object>,
      navigate: navigate == freezed
          ? _value.navigate
          : navigate // ignore: cast_nullable_to_non_nullable
              as void Function(String, [Map<String, String>]),
      pop: pop == freezed
          ? _value.pop
          : pop // ignore: cast_nullable_to_non_nullable
              as void Function()?,
    ));
  }
}

/// @nodoc
abstract class _$RouterModelCopyWith<$Res>
    implements $RouterModelCopyWith<$Res> {
  factory _$RouterModelCopyWith(
          _RouterModel value, $Res Function(_RouterModel) then) =
      __$RouterModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {CustomRouter type,
      RouteInformationParser<Object> informationParser,
      RouterDelegate<Object> delegate,
      void Function(String, [Map<String, String>]) navigate,
      void Function()? pop});
}

/// @nodoc
class __$RouterModelCopyWithImpl<$Res> extends _$RouterModelCopyWithImpl<$Res>
    implements _$RouterModelCopyWith<$Res> {
  __$RouterModelCopyWithImpl(
      _RouterModel _value, $Res Function(_RouterModel) _then)
      : super(_value, (v) => _then(v as _RouterModel));

  @override
  _RouterModel get _value => super._value as _RouterModel;

  @override
  $Res call({
    Object? type = freezed,
    Object? informationParser = freezed,
    Object? delegate = freezed,
    Object? navigate = freezed,
    Object? pop = freezed,
  }) {
    return _then(_RouterModel(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as CustomRouter,
      informationParser: informationParser == freezed
          ? _value.informationParser
          : informationParser // ignore: cast_nullable_to_non_nullable
              as RouteInformationParser<Object>,
      delegate: delegate == freezed
          ? _value.delegate
          : delegate // ignore: cast_nullable_to_non_nullable
              as RouterDelegate<Object>,
      navigate: navigate == freezed
          ? _value.navigate
          : navigate // ignore: cast_nullable_to_non_nullable
              as void Function(String, [Map<String, String>]),
      pop: pop == freezed
          ? _value.pop
          : pop // ignore: cast_nullable_to_non_nullable
              as void Function()?,
    ));
  }
}

/// @nodoc

class _$_RouterModel extends _RouterModel {
  const _$_RouterModel(
      {required this.type,
      required this.informationParser,
      required this.delegate,
      required this.navigate,
      required this.pop})
      : super._();

  @override
  final CustomRouter type;
  @override
  final RouteInformationParser<Object> informationParser;
  @override
  final RouterDelegate<Object> delegate;
  @override
  final void Function(String, [Map<String, String>]) navigate;
  @override
  final void Function()? pop;

  @override
  String toString() {
    return 'RouterModel(type: $type, informationParser: $informationParser, delegate: $delegate, navigate: $navigate, pop: $pop)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RouterModel &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.informationParser, informationParser) ||
                const DeepCollectionEquality()
                    .equals(other.informationParser, informationParser)) &&
            (identical(other.delegate, delegate) ||
                const DeepCollectionEquality()
                    .equals(other.delegate, delegate)) &&
            (identical(other.navigate, navigate) ||
                const DeepCollectionEquality()
                    .equals(other.navigate, navigate)) &&
            (identical(other.pop, pop) ||
                const DeepCollectionEquality().equals(other.pop, pop)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(informationParser) ^
      const DeepCollectionEquality().hash(delegate) ^
      const DeepCollectionEquality().hash(navigate) ^
      const DeepCollectionEquality().hash(pop);

  @JsonKey(ignore: true)
  @override
  _$RouterModelCopyWith<_RouterModel> get copyWith =>
      __$RouterModelCopyWithImpl<_RouterModel>(this, _$identity);
}

abstract class _RouterModel extends RouterModel {
  const factory _RouterModel(
      {required CustomRouter type,
      required RouteInformationParser<Object> informationParser,
      required RouterDelegate<Object> delegate,
      required void Function(String, [Map<String, String>]) navigate,
      required void Function()? pop}) = _$_RouterModel;
  const _RouterModel._() : super._();

  @override
  CustomRouter get type => throw _privateConstructorUsedError;
  @override
  RouteInformationParser<Object> get informationParser =>
      throw _privateConstructorUsedError;
  @override
  RouterDelegate<Object> get delegate => throw _privateConstructorUsedError;
  @override
  void Function(String, [Map<String, String>]) get navigate =>
      throw _privateConstructorUsedError;
  @override
  void Function()? get pop => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RouterModelCopyWith<_RouterModel> get copyWith =>
      throw _privateConstructorUsedError;
}
