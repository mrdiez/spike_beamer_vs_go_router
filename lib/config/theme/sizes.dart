import 'package:flutter/cupertino.dart';
import 'package:gap/gap.dart';

abstract class Sizes {
  Sizes._();

  static const Map<_Sizes, Gap> _gap = {
    zero: Gap(0),
    xxs: Gap(5),
    xs: Gap(10),
    s: Gap(15),
    m: Gap(20),
    l: Gap(25),
    xl: Gap(30),
    xxl: Gap(35)
  };
  static Gap gap(_Sizes size) => _gap[size]!;

  static const Map<_Sizes, EdgeInsets> _padding = {
    zero: EdgeInsets.all(0),
    xxs: EdgeInsets.all(5),
    xs: EdgeInsets.all(10),
    s: EdgeInsets.all(15),
    m: EdgeInsets.all(20),
    l: EdgeInsets.all(25),
    xl: EdgeInsets.all(30),
    xxl: EdgeInsets.all(35)
  };
  static EdgeInsets padding(_Sizes size) => _padding[size]!;
}

enum _Sizes { zero, xxs, xs, s, m, l, xl, xxl }

const zero = _Sizes.zero;
const xxs = _Sizes.xxs;
const xs = _Sizes.xs;
const s = _Sizes.s;
const m = _Sizes.m;
const l = _Sizes.l;
const xl = _Sizes.xl;
const xxl = _Sizes.xxl;
