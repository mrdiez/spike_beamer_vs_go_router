import 'package:spike_beamer_vs_go_router/config/routes.dart';

class Environment {
  // true when flutter run or build commands are launched with "--dart-define=MOCK=true" argument
  static const bool isMock = bool.fromEnvironment('MOCK');

  // true when "flutter run" or "build" commands are launched with "--dart-define=PROD=true" argument
  static const bool isProd = bool.fromEnvironment('PROD');

  static const bool isDev = !isMock && !isProd;

  static const baseUrl = isProd ? 'https://my_domain.com/' : 'https://dev.my_domain.com';

  static CustomRouter router = CustomRouter.goRouter;
  // static CustomRouter router = CustomRouter.beamer;
}
