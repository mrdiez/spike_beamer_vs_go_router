import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:spike_beamer_vs_go_router/modules/auth/auth_routes.dart';
import 'package:spike_beamer_vs_go_router/modules/customer/customer_routes.dart';
import 'package:spike_beamer_vs_go_router/modules/home/home_routes.dart';
import 'package:spike_beamer_vs_go_router/utils/iterable.dart';

enum CustomRouter { goRouter, beamer }

// The error 404 page content
const notFoundScreen = Material(child: Center(child: Text('404')));
const notFoundPage = MaterialPage(child: notFoundScreen);

// The go_router router declaration
final goRouter = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      redirect: (_) => loginRoute,
    ),
    ...authGoRoutes,
    ...homeGoRoutes,
  ],
  errorPageBuilder: (context, state) => notFoundPage,
);

// The beamer router declaration
final beamerRouterDelegate = BeamerDelegate(
  locationBuilder: BeamerLocationBuilder(
    beamLocations: [
      AuthBeamerRoute(),
      HomeBeamerRoute(),
      CustomerBeamerRoute(),
    ],
  ),
  notFoundPage: CustomBeamLocation.pageBuilder(BeamScreen.notFound()),
);

// A homemade class to provide sugar syntax to beamer's routes declaration
class BeamScreen {
  static _defaultParamsAdapter(Map<String, String>? params) => params;
  const BeamScreen(
      {required this.path, this.condition, this.paramsAdapter = _defaultParamsAdapter, required this.builder});
  final Pattern path;
  final Widget? Function(dynamic) builder;
  final bool Function(BeamState)? condition;
  final dynamic Function(Map<String, String>? params) paramsAdapter;
  bool match(BeamState state) => state.uri.path == path || (condition != null ? condition!(state) : false);
  factory BeamScreen.empty() => BeamScreen(path: '', builder: (_) => null);
  factory BeamScreen.notFound() => BeamScreen(path: '', builder: (_) => notFoundScreen);
  factory BeamScreen.nested({required String path, required List<BeamLocation> locations}) => BeamScreen(
      path: path,
      builder: (_) => Beamer(
            routerDelegate: BeamerDelegate(
              locationBuilder: BeamerLocationBuilder(beamLocations: locations),
            ),
          ));
}

// A homemade class to provide sugar syntax to beamer's routes declaration
abstract class CustomBeamLocation extends BeamLocation<BeamState> {
  List<BeamScreen> get screens;
  static BeamPage pageBuilder(BeamScreen screen, [dynamic params]) =>
      BeamPage(key: ValueKey(screen.path), child: Material(child: screen.builder(screen.paramsAdapter(params))));

  @override
  List<Pattern> get pathBlueprints => screens.mapList((screen) => screen.path);

  @override
  List<BeamPage> buildPages(context, state) {
    final _screens = [...screens];
    final params = {...state.queryParameters, ...state.pathParameters};
    final pages = [pageBuilder(_screens.first, params)];
    _screens.removeAt(0);
    for (final screen in _screens) {
      if (screen.match(state)) {
        pages.add(pageBuilder(screen, params));
      }
    }
    return pages;
  }
}
