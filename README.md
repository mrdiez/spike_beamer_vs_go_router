# spike_beamer_vs_go_router

A comparison between the two most famous solution to handle Flutter's navigator 2.0

Used routes are:

* /login
    * /reset_password
    * /sign_up
* /home
    * /dashboard
    * /calendar
    * /customers
        * /list
        * /details